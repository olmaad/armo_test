﻿using ARMO_test.Search;
using System;
using System.Collections.Generic;
using System.IO;

namespace ARMO_test.Model
{
    /// <summary>
    /// Данные результата поиска - каталог или файл. Данные о каталогах являются родительскими для
    /// файлов.
    /// </summary>
    public class SearchResultNode
    {
        /// <summary>
        /// Родительский элемент - каталог, в котором находится этот каталог или файл.
        /// </summary>
        public SearchResultNode Parent { get; private set; }
        /// <summary>
        /// Название найденного каталога или файла.
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Путь до найденного каталога или файла.
        /// </summary>
        public string Path { get; private set; }

        public SearchResultNode(SearchResultNode parent, string name, string path)
        {
            Parent = parent;
            Name = name;
            Path = path;
        }
    }

    /// <summary>
    /// Хранилище результатов поиска. Обновляется при запуске нового поиска, изменениях его состояния и
    /// при появлении новых результатов. Также уведомляет при изменении результатов.
    /// </summary>
    public class SearchResultStorage
    {
        private Dictionary<string, SearchResultNode> mResultNodes;

        /// <summary>
        /// Время текущего поиска (в секундах).
        /// </summary>
        public int SearchTime { get; private set; }
        /// <summary>
        /// Количество полностью обработанных файлов.
        /// </summary>
        public int ProcessedFileCount { get; private set; }

        /// <summary>
        /// Событие изменения времени текущего поиска.
        /// </summary>
        public event EventHandler<int> SearchTimeChanged;
        /// <summary>
        /// Событие изменения количества обработанных файлов.
        /// </summary>
        public event EventHandler<int> ProcessedFileCountChanged;
        /// <summary>
        /// Событие добавления нового результата поиска.
        /// </summary>
        public event EventHandler<SearchResultNode> ResultNodeAdded;

        public SearchResultStorage(SearchRunner runner)
        {
            if (runner == null)
            {
                throw new ArgumentNullException("Runner can't be null");
            }

            mResultNodes = new Dictionary<string, SearchResultNode>();

            // Подписка на события изменения состояния поиска
            runner.SearchStarted += HandleSearchStarted;
            runner.FileProcessingFinished += HandleFileProcessingFinished;
            runner.FileFound += HandleFileFound;
            runner.SearchTimeTick += HandleSearchTimeTick;
        }

        /// <summary>
        /// Обработчик события запуска нового поиска. Очищает все результаты и уведомляет об этом.
        /// </summary>
        private void HandleSearchStarted(object sender, EventArgs e)
        {
            SearchTime = 0;
            ProcessedFileCount = 0;
            mResultNodes.Clear();

            SearchTimeChanged?.Invoke(this, SearchTime);
            ProcessedFileCountChanged?.Invoke(this, ProcessedFileCount);
        }

        /// <summary>
        /// Обработчик события завершения обработки файла. Изменяет счетчик обработанных файлов
        /// и уведомляет об этом.
        /// </summary>
        private void HandleFileProcessingFinished(object sender, EventArgs e)
        {
            ProcessedFileCount += 1;

            ProcessedFileCountChanged?.Invoke(this, ProcessedFileCount);
        }

        /// <summary>
        /// Обработчик события нахождения нового файла. Добавляет новый результат, родительские элементы
        /// и уведомляет подписчиков.
        /// </summary>
        /// <param name="path">Путь к найденному файлу.</param>
        private void HandleFileFound(object sender, string path)
        {
            string[] nodePathParts = path.Split(Path.DirectorySeparatorChar);

            SearchResultNode parent = null;
            string nodePath = "";

            // Создание необходимых родительских элементов и нового результата.
            foreach (var it in nodePathParts)
            {
                nodePath = string.IsNullOrEmpty(nodePath) ? (it) : (nodePath + Path.DirectorySeparatorChar + it);

                SearchResultNode node;

                if (mResultNodes.ContainsKey(it))
                {
                    node = mResultNodes[it];
                }
                else
                {
                    node = new SearchResultNode(parent, it, nodePath);

                    mResultNodes.Add(it, node);

                    ResultNodeAdded?.Invoke(this, node);
                }

                parent = node;
            }
        }

        /// <summary>
        /// Обработчик таймера времени поиска. Обновляет количество времени, которое идет поиск
        /// и уведомляет подписчиков.
        /// </summary>
        private void HandleSearchTimeTick(object sender, EventArgs e)
        {
            SearchTime += 1;

            SearchTimeChanged?.Invoke(this, SearchTime);
        }
    }
}
