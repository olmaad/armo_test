﻿namespace ARMO_test
{
    /// <summary>
    /// Позволяет сохранять и загружать параметры поиска между запусками.
    /// </summary>
    public class Settings
    {
        public static void Save(string baseDirectoryPath, string filenamePattern, string innerText)
        {
            Properties.Settings.Default.lastBaseDirectoryPath = baseDirectoryPath;
            Properties.Settings.Default.lastFilenamePattern = filenamePattern;
            Properties.Settings.Default.lastInnerText = innerText;

            Properties.Settings.Default.Save();
        }

        public static void Restore(out string baseDirectoryPath, out string filenamePattern, out string innerText)
        {
            baseDirectoryPath = Properties.Settings.Default.lastBaseDirectoryPath;
            filenamePattern = Properties.Settings.Default.lastFilenamePattern;
            innerText = Properties.Settings.Default.lastInnerText;
        }
    }
}
