﻿namespace ARMO_test.Controller
{
    /// <summary>
    /// Интерфейс контроллера главного окна. Содержит обработчики событий от пользователя.
    /// </summary>
    public interface IMainController
    {
        /// <summary>
        /// Обработчик изменения базовой директории. С помощью него контроллер может
        /// проинформировать пользователя о несуществующей директории, например.
        /// </summary>
        /// <param name="path">Путь к базовой директории</param>
        void BaseDirectoryChanged(string path);

        /// <summary>
        /// Обработчик события запуска/остановки поиска.
        /// </summary>
        void StartStopSearch();
        /// <summary>
        /// Обработчик события приостановки поиска.
        /// </summary>
        void PauseSearch();

        /// <summary>
        /// Обработчик события закрытия главного окна.
        /// </summary>
        void HandleClose();
    }
}
