﻿using ARMO_test.Model;
using ARMO_test.Search;
using ARMO_test.View;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ARMO_test.Controller
{
    public class MainController : IMainController
    {
        private object mHandlersLock;
        private IMainView mView;
        private SearchRunner mRunner;
        private SearchResultStorage mResultStorage;

        public MainController(IMainView view, SearchRunner runner, SearchResultStorage resultStorage)
        {
            mHandlersLock = new object();

            mView = view ?? throw new ArgumentNullException("View can't be null");
            mRunner = runner ?? throw new ArgumentNullException("Runner can't be null");
            mResultStorage = resultStorage ?? throw new ArgumentNullException("Result storage can't be null");

            mView.SetController(this);

            // Загрузка предыдущих параметров поиска
            Settings.Restore(out var baseDirectoryPath, out var filenamePattern, out var innerText);

            mView.BaseDirectoryPath = baseDirectoryPath;
            mView.FilenamePattern = filenamePattern;
            mView.InnerText = innerText;
            
            // Подписка на события изменения состояния поиска
            mRunner.SearchStopped += HandleSearchStopped;
            mRunner.FileProcessingStarted += HandleFileProcessingStarted;
            mRunner.FileProcessingFinished += HandleFileProcessingFinished;

            // Подписка на события изменения результатов поиска
            mResultStorage.ProcessedFileCountChanged += HandleProcessedFileCountChanged;
            mResultStorage.ResultNodeAdded += HandleResultNodeAdded;
            mResultStorage.SearchTimeChanged += HandleSearchTimeChanged;
        }

        public void BaseDirectoryChanged(string path)
        {
            // Проверка существования и доступа к базовой директории
            try
            {
                if (!Directory.Exists(path))
                {
                    mView.Messages = mView.Messages | MessageType.MessageTypeNoSuchDirectory;
                }
                else
                {
                    mView.Messages = mView.Messages & ~MessageType.MessageTypeNoSuchDirectory;
                }

                mView.Messages = mView.Messages & ~MessageType.MessageTypeBaseDirectoryNoAccess;
            }
            catch (Exception)
            {
                mView.Messages = mView.Messages | MessageType.MessageTypeBaseDirectoryNoAccess;
            }
        }

        /// <summary>
        /// Изменяет состояния окна, сообщений и запускает новый поиск, если нужно.
        /// </summary>
        public void StartStopSearch()
        {
            if (mRunner.IsPaused)
            {
                mView.SetMode(MainViewMode.ModeRunning);
                mView.Messages = MessageType.MessageTypeNoMessage;

                mRunner.ResumeSearch();
            }
            else if (mRunner.IsRunningSearch)
            {
                mView.SetMode(MainViewMode.ModeStopped);
                mView.Messages = MessageType.MessageSearchFinished;

                mRunner.StopSearch();
            }
            else
            {
                // Поиск не начинается, если стартовая директория не существует, если к ней нет доступа, или если не заданы критерии поиска
                try
                {
                    if (!Directory.Exists(mView.BaseDirectoryPath))
                    {
                        mView.Messages = mView.Messages | MessageType.MessageTypeNoSuchDirectory;
                        return;
                    }
                    else
                    {
                        mView.Messages = mView.Messages & ~MessageType.MessageTypeNoSuchDirectory;
                    }

                    mView.Messages = mView.Messages & ~MessageType.MessageTypeBaseDirectoryNoAccess;
                }
                catch (Exception)
                {
                    mView.Messages = mView.Messages | MessageType.MessageTypeBaseDirectoryNoAccess;
                    return;
                }

                if (string.IsNullOrEmpty(mView.FilenamePattern) && string.IsNullOrEmpty(mView.InnerText))
                {
                    mView.Messages = mView.Messages | MessageType.MessageTypeNoParameters;
                    return;
                }

                Regex filenameRegex;

                try
                {
                    if (string.IsNullOrEmpty(mView.FilenamePattern))
                    {
                        filenameRegex = new Regex(".*");
                    }
                    else
                    {
                        filenameRegex = new Regex(mView.FilenamePattern);
                    }
                }
                catch (Exception)
                {
                    mView.Messages = mView.Messages | MessageType.MessageTypePatternError;
                    return;
                }

                mView.SetMode(MainViewMode.ModeRunning);
                mView.Messages = MessageType.MessageTypeNoMessage;

                mView.ResetResults();

                // Создание нового поиска по директории и передача его в исполнитель
                ISearch search = new DirectorySearch(mView.BaseDirectoryPath, filenameRegex, mView.InnerText);

                mRunner.RunSearch(search);
            }
        }

        public void PauseSearch()
        {
            mView.SetMode(MainViewMode.ModePaused);
            mView.Messages = MessageType.MessageSearchPaused;

            mRunner.PauseSearch();
        }
        
        public void HandleClose()
        {
            // Отписка от событий и сохранение параметров
            lock (mHandlersLock)
            {
                mRunner.SearchStopped -= HandleSearchStopped;
                mRunner.FileProcessingStarted -= HandleFileProcessingStarted;
                mRunner.FileProcessingFinished -= HandleFileProcessingFinished;

                mResultStorage.ProcessedFileCountChanged -= HandleProcessedFileCountChanged;
                mResultStorage.ResultNodeAdded -= HandleResultNodeAdded;
                mResultStorage.SearchTimeChanged -= HandleSearchTimeChanged;

                mRunner.StopSearch();

                Settings.Save(mView.BaseDirectoryPath, mView.FilenamePattern, mView.InnerText);
            }
        }

        /// <summary>
        /// Обработчик события завершения поиска. Уведомляет пользователя
        /// о завершении поиска.
        /// </summary>
        private void HandleSearchStopped(object sender, EventArgs e)
        {
            lock (mHandlersLock)
            {
                mView.SetMode(MainViewMode.ModeStopped);
                mView.Messages = MessageType.MessageSearchFinished;

                mView.SetCurrentProcessingFilename(null);
            }
        }

        /// <summary>
        /// Обработчик события начала обработки файла. Меняет отображаемое
        /// имя обрабатываемого файла.
        /// </summary>
        private void HandleFileProcessingStarted(object sender, string path)
        {
            lock (mHandlersLock)
            {
                string filename = Path.GetFileName(path);

                mView.SetCurrentProcessingFilename(filename);
            }
        }

        /// <summary>
        /// Обработчик события конца обработки файла. Перестает отображать
        /// имя обрабатываемого файла.
        /// </summary>
        private void HandleFileProcessingFinished(object sender, EventArgs e)
        {
            lock (mHandlersLock)
            {
                mView.SetCurrentProcessingFilename(null);
            }
        }

        /// <summary>
        /// Обработчик события изменения количества обработанных файлов. Меняет отображаемое
        /// количество.
        /// </summary>
        private void HandleProcessedFileCountChanged(object sender, int count)
        {
            lock (mHandlersLock)
            {
                mView.SetProcessedFileCount(count);
            }
        }

        /// <summary>
        /// Обработчик события появления нового результата. Добавляет новый элемент к результатам
        /// и все родительские.
        /// </summary>
        /// <param name="node">Новый результат с результат с ссылкой на родительские элементы.</param>
        private void HandleResultNodeAdded(object sender, SearchResultNode node)
        {
            lock (mHandlersLock)
            {
                CreateNodes(node);
            }
        }

        /// <summary>
        /// Обработчик события изменения количества времени, которое длится поиск. Меняет отображаемое
        /// время
        /// </summary>
        /// <param name="time">Новое значение времени (в секундах.)</param>
        private void HandleSearchTimeChanged(object sender, int time)
        {
            lock (mHandlersLock)
            {
                if (mView == null)
                {
                    return;
                }

                mView.SetSearchTime(time);
            }
        }

        /// <summary>
        /// Рекурсивно добавляет отображение результатов и родительских элементов.
        /// </summary>
        /// <param name="node">Результат с ссылкой на родительские элементы.</param>
        private void CreateNodes(SearchResultNode node)
        {
            if (node == null)
            {
                return;
            }

            CreateNodes(node.Parent);

            mView.AddResult(node.Parent?.Path, node.Path, node.Name);
        }
    }
}
