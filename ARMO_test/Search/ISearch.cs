﻿using System;

namespace ARMO_test.Search
{
    /// <summary>
    /// Интерфейс поиска. Позволяет узнавать статус и продолжать поиск.
    /// </summary>
    public interface ISearch
    {
        /// <summary>
        /// Событие начала обработки файла.
        /// </summary>
        event EventHandler<string> FileProcessingStarted;
        /// <summary>
        /// Событие конца обработки файла.
        /// </summary>
        event EventHandler FileProcessingFinished;
        /// <summary>
        /// Событие нахождения нового подходящего файла.
        /// </summary>
        event EventHandler<string> FileFound;

        /// <summary>
        /// Указывает, завершен ли поиск.
        /// </summary>
        bool Finished { get; }

        /// <summary>
        /// Продолжает поиск, выполняя одно действие. Например - проверяет одну строку на содержимое, или имя файла.
        /// </summary>
        void Continue();
    }
}
