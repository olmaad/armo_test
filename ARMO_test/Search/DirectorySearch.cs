﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ARMO_test.Search
{
    /// <summary>
    /// Поиск по директории. Проходит по дочерним директориям и файлам в текущей директории.
    /// </summary>
    public class DirectorySearch : ISearch
    {
        private bool mInited;
        private string mDirectoryPath;
        private Regex mFilenameRegex;
        private string mInnerText;
        private List<ISearch> mSearches;
        
        public event EventHandler<string> FileProcessingStarted;
        public event EventHandler FileProcessingFinished;
        public event EventHandler<string> FileFound;
        
        public bool Finished { get; private set; }

        public DirectorySearch(string directoryPath, Regex filenameRegex, string innerText)
        {
            mInited = false;
            mSearches = new List<ISearch>();

            mDirectoryPath = directoryPath;
            mFilenameRegex = filenameRegex;
            mInnerText = innerText;

            Finished = false;
        }

        public void Continue()
        {
            // Создание поисков по дочерним директориям и по файлам
            if (!mInited)
            {
                try
                {
                    string[] directories = Directory.GetDirectories(mDirectoryPath);

                    foreach (var it in directories)
                    {
                        ISearch search = new DirectorySearch(it, mFilenameRegex, mInnerText);

                        search.FileProcessingStarted += FileProcessingStarted;
                        search.FileProcessingFinished += FileProcessingFinished;
                        search.FileFound += FileFound;

                        mSearches.Add(search);
                    }

                    string[] files = Directory.GetFiles(mDirectoryPath);

                    foreach (var it in files)
                    {
                        ISearch search = new FileSearch(it, mFilenameRegex, mInnerText);

                        search.FileProcessingStarted += FileProcessingStarted;
                        search.FileProcessingFinished += FileProcessingFinished;
                        search.FileFound += FileFound;

                        mSearches.Add(search);
                    }
                }
                catch (Exception)
                {
                    // Поиск считается завершенным, если нет доступа к директории
                    Finished = true;
                }

                mInited = true;

                return;
            }

            // Поиск считается завершенным, когда завершен поиск по дочерним директориям и по файлам
            if (!mSearches.Any())
            {
                Finished = true;
                return;
            }

            // Обработка дочерних поисков
            mSearches.First().Continue();

            // И удаление завершенных
            if (mSearches.First().Finished)
            {
                mSearches.RemoveAt(0);
            }
        }
    }
}
