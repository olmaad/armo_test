﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ARMO_test.Search
{
    /// <summary>
    /// Файловый поиск. Проверяет имя файла на соответствие шаблону имени и ищет содержимое, если указано
    /// </summary>
    public class FileSearch : ISearch
    {
        /// <summary>
        /// Состояние поиска.
        /// </summary>
        private enum State
        {
            /// <summary>
            /// Состояние - проверка имени.
            /// </summary>
            StateCheckingName,
            /// <summary>
            /// Состояние - проверка содержимого.
            /// </summary>
            StateCheckingContent,
            /// <summary>
            /// Состояние - файл удовлетворяет критерям поиска, поиск завершен.
            /// </summary>
            StateFound,
            /// <summary>
            /// Состояние - файл не удовлетворяет критериям поиска, поиск завершен.
            /// </summary>
            StateNotFound
        }

        private State mState;
        private string mFilePath;
        private Regex mFilenameRegex;
        private string mInnerText;
        private StreamReader mFileReader;

        public event EventHandler<string> FileProcessingStarted;
        public event EventHandler FileProcessingFinished;
        public event EventHandler<string> FileFound;

        public bool Finished { get; private set; }

        public FileSearch(string filePath, Regex filenameRegex, string innerText)
        {
            mState = State.StateCheckingName;
            mFilePath = filePath;
            mFilenameRegex = filenameRegex;
            mInnerText = innerText;

            Finished = false;
        }

        /// <summary>
        /// Закрывает файл, если открыт.
        /// </summary>
        ~FileSearch()
        {
            if (mFileReader != null)
            {
                mFileReader.Close();
                mFileReader = null;
            }
        }

        public void Continue()
        {
            switch (mState)
            {
                // Проверка имени файла на соответствие шаблону. Если подходит - переход к проверке содержимого
                // или к результату, в зависимости от наличия ожидаемого содержимого
                case State.StateCheckingName:
                    {
                        FileProcessingStarted?.Invoke(this, mFilePath);

                        string filename = Path.GetFileName(mFilePath);

                        if (mFilenameRegex.IsMatch(filename))
                        {
                            if (string.IsNullOrEmpty(mInnerText))
                            {
                                mState = State.StateFound;
                            }
                            else
                            {
                                mState = State.StateCheckingContent;
                            }
                        }
                        else
                        {
                            mState = State.StateNotFound;
                        }

                        break;
                    }
                // Проверка содержания заданной подстроки в каждой строке файла
                case State.StateCheckingContent:
                    {
                        try
                        {
                            if (mFileReader == null)
                            {
                                mFileReader = new StreamReader(mFilePath);
                            }

                            string line = mFileReader.ReadLine();

                            if (line == null)
                            {
                                mState = State.StateNotFound;

                                mFileReader.Close();
                                mFileReader = null;

                                break;
                            }

                            if (line.Contains(mInnerText))
                            {
                                mState = State.StateFound;
                            }
                        }
                        catch(Exception)
                        {
                            mState = State.StateNotFound;
                        }

                        break;
                    }
                // Уведомление о нахождении файла и о конце поиска
                case State.StateFound:
                    {
                        Finished = true;

                        FileFound?.Invoke(this, mFilePath);
                        FileProcessingFinished?.Invoke(this, null);

                        break;
                    }
                // Уведомление о конце поиска
                case State.StateNotFound:
                    {
                        Finished = true;

                        FileProcessingFinished?.Invoke(this, null);

                        break;
                    }
            }
        }
    }
}
