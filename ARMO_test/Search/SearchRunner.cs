﻿using System;
using System.Threading;

namespace ARMO_test.Search
{
    /// <summary>
    /// Исполнитель поиска. Проводит поиск в фоне.
    /// </summary>
    public class SearchRunner
    {
        /// <summary>
        /// Состояние исполнителя.
        /// </summary>
        private enum State
        {
            /// <summary>
            /// Состояние - не запущен.
            /// </summary>
            StateNotRunning,
            /// <summary>
            /// Состояние - поиск исполняется.
            /// </summary>
            StateRunning,
            /// <summary>
            /// Состояние - ожидает задачу.
            /// </summary>
            StateWaiting,
            /// <summary>
            /// Состояние - поиск приостановлен.
            /// </summary>
            StatePaused,
            /// <summary>
            /// Состояние - исполнитель остановлен.
            /// </summary>
            StateStopped
        }

        private object mStateLock;
        private object mCurrentSearchLock;

        private AutoResetEvent mWakeupEvent;

        private State mState;
        private Thread mThread;
        private ISearch mCurrentSearch;
        private Timer mTimer;

        /// <summary>
        /// Указывает, есть у исполнителя задача.
        /// </summary>
        public bool IsRunningSearch
        {
            get
            {
                lock (mCurrentSearchLock)
                {
                    return (mCurrentSearch != null);
                }
            }
        }

        /// <summary>
        /// Указывает, поставлен ли поиск на паузу.
        /// </summary>
        public bool IsPaused
        {
            get
            {
                lock (mStateLock)
                {
                    return (mState == State.StatePaused);
                }
            }
        }

        /// <summary>
        /// Событие запуска нового поиска.
        /// </summary>
        public event EventHandler SearchStarted;
        /// <summary>
        /// Событие приостановки поиска.
        /// </summary>
        public event EventHandler SearchPaused;
        /// <summary>
        /// Событие возобновления поиска.
        /// </summary>
        public event EventHandler SearchResumed;
        /// <summary>
        /// Событие завершения поиска.
        /// </summary>
        public event EventHandler SearchStopped;
        /// <summary>
        /// Событие начала обработки файла.
        /// </summary>
        public event EventHandler<string> FileProcessingStarted;
        /// <summary>
        /// Событие завершения обработки файла.
        /// </summary>
        public event EventHandler FileProcessingFinished;
        /// <summary>
        /// Событие нахождения файла.
        /// </summary>
        public event EventHandler<string> FileFound;
        /// <summary>
        /// Событие таймера
        /// </summary>
        public event EventHandler SearchTimeTick;

        public SearchRunner()
        {
            mStateLock = new object();
            mCurrentSearchLock = new object();

            mWakeupEvent = new AutoResetEvent(false);

            mState = State.StateNotRunning;
            mThread = new Thread(RunnerThread);
            mTimer = new Timer(new TimerCallback(HandleTimerTick));
        }

        /// <summary>
        /// Запускает исполнитель и переводит его в состояние ожидания задачи.
        /// </summary>
        public void Start()
        {
            lock (this)
            {
                if (mState != State.StateNotRunning)
                {
                    return;
                }

                mState = State.StateWaiting;
            }

            mThread.Start();
        }

        /// <summary>
        /// Останавливает исполнитель и ждет его завершения.
        /// </summary>
        public void Stop()
        {
            lock (mStateLock)
            {
                if (mState == State.StateStopped)
                {
                    return;
                }

                mState = State.StateStopped;
            }

            mWakeupEvent.Set();

            mThread.Join();
        }

        /// <summary>
        /// Запускает новую задачу поиска, переводит исполнитель в состояние выполнения задачи
        /// и уведомляет подписчиков.
        /// </summary>
        /// <param name="search">Поиск, который нужно выполнить.</param>
        public void RunSearch(ISearch search)
        {
            if (search == null)
            {
                throw new ArgumentNullException("Search can't be null");
            }

            SearchStarted?.Invoke(this, null);

            search.FileProcessingStarted += FileProcessingStarted;
            search.FileProcessingFinished += FileProcessingFinished;
            search.FileFound += FileFound;

            lock (mCurrentSearchLock)
            {
                mCurrentSearch = search;

                lock (mStateLock)
                {
                    mState = State.StateRunning;
                }
            }

            mWakeupEvent.Set();

            mTimer.Change(1000, 1000);
        }

        /// <summary>
        /// Переводит исполнитель в состояние паузы и уведомляет подписчиков.
        /// </summary>
        public void PauseSearch()
        {
            lock (mStateLock)
            {
                if (mState != State.StateRunning)
                {
                    return;
                }

                mState = State.StatePaused;
            }

            SearchPaused?.Invoke(this, null);

            mTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        /// <summary>
        /// Переводит исполнитель обратно в состояние выполнения задачи и уведомляет подписчиков.
        /// </summary>
        public void ResumeSearch()
        {
            lock (mStateLock)
            {
                if (mState != State.StatePaused)
                {
                    return;
                }

                mState = State.StateRunning;
            }
            mWakeupEvent.Set();

            SearchResumed?.Invoke(this, null);

            mTimer.Change(1000, 1000);
        }

        /// <summary>
        /// Останавливает выполнение текущей задачи и уведомляет подписчиков.
        /// </summary>
        public void StopSearch()
        {
            SearchStopped?.Invoke(this, null);

            lock (mCurrentSearchLock)
            {
                mCurrentSearch = null;

                lock (mStateLock)
                {
                    if (mState == State.StatePaused)
                    {
                        mState = State.StateRunning;
                    }
                }
            }

            mTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        /// <summary>
        /// Функция потока исполнителя, которая и выполняет задачу.
        /// </summary>
        private void RunnerThread()
        {
            while (true)
            {
                try
                {
                    Monitor.Enter(mStateLock);

                    switch (mState)
                    {
                        // Выход из потока при остановке
                        case State.StateStopped:
                            {
                                return;
                            }
                        // Переход в режим ожидания
                        case State.StateWaiting:
                            {
                                Monitor.Exit(mStateLock);

                                mWakeupEvent.WaitOne();

                                break;
                            }
                        // Выполнение текущей задачи, или переход в режим ожидания, если она завершена
                        case State.StateRunning:
                            {
                                Monitor.Exit(mStateLock);

                                Monitor.Enter(mCurrentSearchLock);

                                if (mCurrentSearch == null)
                                {
                                    Monitor.Exit(mCurrentSearchLock);

                                    mTimer.Change(Timeout.Infinite, Timeout.Infinite);
                                    SearchStopped?.Invoke(this, null);

                                    Monitor.Enter(mStateLock);

                                    mState = State.StateWaiting;

                                    break;
                                }

                                mCurrentSearch.Continue();

                                if (mCurrentSearch.Finished)
                                {
                                    mCurrentSearch = null;
                                }

                                break;
                            }
                        // Переход в режим паузы
                        case State.StatePaused:
                            {
                                Monitor.Exit(mStateLock);

                                mWakeupEvent.WaitOne();

                                Monitor.Enter(mStateLock);

                                mState = State.StateRunning;

                                break;
                            }
                    }
                }
                catch (Exception)
                {
                    if (!Monitor.IsEntered(mCurrentSearchLock))
                    {
                        Monitor.Enter(mCurrentSearchLock);
                    }

                    mCurrentSearch = null;

                    Monitor.Exit(mCurrentSearchLock);
                }
                finally
                {
                    if (Monitor.IsEntered(mCurrentSearchLock))
                    {
                        Monitor.Exit(mCurrentSearchLock);
                    }

                    if (Monitor.IsEntered(mStateLock))
                    {
                        Monitor.Exit(mStateLock);
                    }
                }
            }
        }
        
        private void HandleTimerTick(object o)
        {
            SearchTimeTick?.Invoke(this, null);
        }
    }
}
