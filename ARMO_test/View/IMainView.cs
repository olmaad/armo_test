﻿using ARMO_test.Controller;

namespace ARMO_test.View
{
    /// <summary>
    /// Типы статусных сообщений.
    /// </summary>
    public enum MessageType
    {
        MessageTypeNoMessage = 0,
        MessageTypeNoSuchDirectory = 1,
        MessageSearchFinished = 2,
        MessageSearchPaused = 4,
        MessageTypeNoParameters = 8,
        MessageTypePatternError = 16,
        MessageTypeBaseDirectoryNoAccess = 32
    }

    /// <summary>
    /// Режимы главного окна.
    /// </summary>
    public enum MainViewMode
    {
        /// <summary>
        /// Главное окно - поиск остановлен, можно начинать новый. 
        /// </summary>
        ModeStopped,
        /// <summary>
        /// Главное окно - поиск запущен, можно приостановить или остановить.
        /// </summary>
        ModeRunning,
        /// <summary>
        /// Главное окно - поиск приостановлен, можно продолжить.
        /// </summary>
        ModePaused
    }

    /// <summary>
    /// Интерфейс главного окна. Позволяет менять информацию, отображаемую пользователю.
    /// </summary>
    public interface IMainView
    {
        /// <summary>
        /// Путь к начальной директории поиска.
        /// </summary>
        string BaseDirectoryPath { get; set; }
        /// <summary>
        /// Шаблон имени файла.
        /// </summary>
        string FilenamePattern { get; set; }
        /// <summary>
        /// Ожидаемое содержимое файла.
        /// </summary>
        string InnerText { get; set; }
        /// <summary>
        /// Статусные сообщения.
        /// </summary>
        MessageType Messages { get; set; }

        /// <summary>
        /// Устанавливает контроллер главного окна, через который пользователь взаимодействует с приложением.
        /// </summary>
        /// <param name="value"></param>
        void SetController(IMainController value);

        /// <summary>
        /// Устанавливает отображаемое имя текущего обрабатываемого файла, или удаляет его.
        /// </summary>
        /// <param name="value">Имя файла. null или пустая строка для удаления.</param>
        void SetCurrentProcessingFilename(string value);
        /// <summary>
        /// Устанавливает отображаемое количество обработанных файлов.
        /// </summary>
        /// <param name="value">Количество обработанных файлов.</param>
        void SetProcessedFileCount(int value);
        /// <summary>
        /// Устанавливает отображаемое время, которое продолжается поиск.
        /// </summary>
        /// <param name="value">Время, которое продолжается поиск.</param>
        void SetSearchTime(int value);
        /// <summary>
        /// Устанавливает режим главного окна.
        /// </summary>
        /// <param name="value">Режим главного окна.</param>
        void SetMode(MainViewMode value);
        
        /// <summary>
        /// Добавляет новый отображаемый элемент к результатам поиска.
        /// </summary>
        /// <param name="parent">Ключ родительского элемента.</param>
        /// <param name="key">Ключ элемента. Используется для привязки других элементов этому.</param>
        /// <param name="name">Отображаемый текст результата.</param>
        void AddResult(string parent, string key, string name);
        /// <summary>
        /// Очищает отображение результатов поиска.
        /// </summary>
        void ResetResults();
    }
}
