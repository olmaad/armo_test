﻿namespace ARMO_test.View
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parametersGroupBox = new System.Windows.Forms.GroupBox();
            this.messageLabel = new System.Windows.Forms.Label();
            this.pauseButton = new System.Windows.Forms.Button();
            this.startStopButton = new System.Windows.Forms.Button();
            this.innerTextTextBox = new System.Windows.Forms.TextBox();
            this.innerTextLabel = new System.Windows.Forms.Label();
            this.fileNamePatternTextBox = new System.Windows.Forms.TextBox();
            this.fileNamePatternLabel = new System.Windows.Forms.Label();
            this.baseDirectoryPathLabel = new System.Windows.Forms.Label();
            this.browseBaseDirectoryButton = new System.Windows.Forms.Button();
            this.baseDirectoryPathTextBox = new System.Windows.Forms.TextBox();
            this.resultsGroupBox = new System.Windows.Forms.GroupBox();
            this.resultsTreeView = new System.Windows.Forms.TreeView();
            this.mainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.timeToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.fileCountToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.currentFileToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.messagesToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.baseFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parametersGroupBox.SuspendLayout();
            this.resultsGroupBox.SuspendLayout();
            this.mainStatusStrip.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // parametersGroupBox
            // 
            this.parametersGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.parametersGroupBox.Controls.Add(this.messageLabel);
            this.parametersGroupBox.Controls.Add(this.pauseButton);
            this.parametersGroupBox.Controls.Add(this.startStopButton);
            this.parametersGroupBox.Controls.Add(this.innerTextTextBox);
            this.parametersGroupBox.Controls.Add(this.innerTextLabel);
            this.parametersGroupBox.Controls.Add(this.fileNamePatternTextBox);
            this.parametersGroupBox.Controls.Add(this.fileNamePatternLabel);
            this.parametersGroupBox.Controls.Add(this.baseDirectoryPathLabel);
            this.parametersGroupBox.Controls.Add(this.browseBaseDirectoryButton);
            this.parametersGroupBox.Controls.Add(this.baseDirectoryPathTextBox);
            this.parametersGroupBox.Location = new System.Drawing.Point(12, 27);
            this.parametersGroupBox.Name = "parametersGroupBox";
            this.parametersGroupBox.Size = new System.Drawing.Size(760, 134);
            this.parametersGroupBox.TabIndex = 0;
            this.parametersGroupBox.TabStop = false;
            this.parametersGroupBox.Text = "Параметры";
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Location = new System.Drawing.Point(6, 103);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(0, 13);
            this.messageLabel.TabIndex = 9;
            // 
            // pauseButton
            // 
            this.pauseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pauseButton.Location = new System.Drawing.Point(598, 98);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(75, 25);
            this.pauseButton.TabIndex = 8;
            this.pauseButton.Text = "Пауза";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Visible = false;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // startStopButton
            // 
            this.startStopButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.startStopButton.Location = new System.Drawing.Point(679, 98);
            this.startStopButton.Name = "startStopButton";
            this.startStopButton.Size = new System.Drawing.Size(75, 25);
            this.startStopButton.TabIndex = 7;
            this.startStopButton.Text = "Старт";
            this.startStopButton.UseVisualStyleBackColor = true;
            this.startStopButton.Click += new System.EventHandler(this.startStopButton_Click);
            // 
            // innerTextTextBox
            // 
            this.innerTextTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.innerTextTextBox.Location = new System.Drawing.Point(172, 72);
            this.innerTextTextBox.Name = "innerTextTextBox";
            this.innerTextTextBox.Size = new System.Drawing.Size(582, 20);
            this.innerTextTextBox.TabIndex = 6;
            // 
            // innerTextLabel
            // 
            this.innerTextLabel.AutoSize = true;
            this.innerTextLabel.Location = new System.Drawing.Point(6, 75);
            this.innerTextLabel.MinimumSize = new System.Drawing.Size(160, 0);
            this.innerTextLabel.Name = "innerTextLabel";
            this.innerTextLabel.Size = new System.Drawing.Size(160, 13);
            this.innerTextLabel.TabIndex = 5;
            this.innerTextLabel.Text = "Текст в файле";
            // 
            // fileNamePatternTextBox
            // 
            this.fileNamePatternTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileNamePatternTextBox.Location = new System.Drawing.Point(172, 45);
            this.fileNamePatternTextBox.Name = "fileNamePatternTextBox";
            this.fileNamePatternTextBox.Size = new System.Drawing.Size(582, 20);
            this.fileNamePatternTextBox.TabIndex = 4;
            // 
            // fileNamePatternLabel
            // 
            this.fileNamePatternLabel.AutoSize = true;
            this.fileNamePatternLabel.Location = new System.Drawing.Point(6, 48);
            this.fileNamePatternLabel.MinimumSize = new System.Drawing.Size(160, 0);
            this.fileNamePatternLabel.Name = "fileNamePatternLabel";
            this.fileNamePatternLabel.Size = new System.Drawing.Size(160, 13);
            this.fileNamePatternLabel.TabIndex = 3;
            this.fileNamePatternLabel.Text = "Шаблон имени файла";
            // 
            // baseDirectoryPathLabel
            // 
            this.baseDirectoryPathLabel.AutoSize = true;
            this.baseDirectoryPathLabel.Location = new System.Drawing.Point(6, 22);
            this.baseDirectoryPathLabel.MinimumSize = new System.Drawing.Size(160, 0);
            this.baseDirectoryPathLabel.Name = "baseDirectoryPathLabel";
            this.baseDirectoryPathLabel.Size = new System.Drawing.Size(160, 13);
            this.baseDirectoryPathLabel.TabIndex = 2;
            this.baseDirectoryPathLabel.Text = "Путь начальной директории";
            this.baseDirectoryPathLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // browseBaseDirectoryButton
            // 
            this.browseBaseDirectoryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseBaseDirectoryButton.Location = new System.Drawing.Point(679, 17);
            this.browseBaseDirectoryButton.Name = "browseBaseDirectoryButton";
            this.browseBaseDirectoryButton.Size = new System.Drawing.Size(75, 23);
            this.browseBaseDirectoryButton.TabIndex = 1;
            this.browseBaseDirectoryButton.Text = "Открыть...";
            this.browseBaseDirectoryButton.UseVisualStyleBackColor = true;
            this.browseBaseDirectoryButton.Click += new System.EventHandler(this.browseBaseDirectoryButton_Click);
            // 
            // baseDirectoryPathTextBox
            // 
            this.baseDirectoryPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.baseDirectoryPathTextBox.Location = new System.Drawing.Point(172, 19);
            this.baseDirectoryPathTextBox.Name = "baseDirectoryPathTextBox";
            this.baseDirectoryPathTextBox.Size = new System.Drawing.Size(501, 20);
            this.baseDirectoryPathTextBox.TabIndex = 0;
            this.baseDirectoryPathTextBox.TextChanged += new System.EventHandler(this.baseDirectoryPathTextBox_TextChanged);
            // 
            // resultsGroupBox
            // 
            this.resultsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsGroupBox.Controls.Add(this.resultsTreeView);
            this.resultsGroupBox.Location = new System.Drawing.Point(12, 167);
            this.resultsGroupBox.Name = "resultsGroupBox";
            this.resultsGroupBox.Size = new System.Drawing.Size(760, 387);
            this.resultsGroupBox.TabIndex = 1;
            this.resultsGroupBox.TabStop = false;
            this.resultsGroupBox.Text = "Результаты";
            // 
            // resultsTreeView
            // 
            this.resultsTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsTreeView.Location = new System.Drawing.Point(6, 19);
            this.resultsTreeView.Name = "resultsTreeView";
            this.resultsTreeView.Size = new System.Drawing.Size(748, 362);
            this.resultsTreeView.TabIndex = 0;
            // 
            // mainStatusStrip
            // 
            this.mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.timeToolStripStatusLabel,
            this.fileCountToolStripStatusLabel,
            this.currentFileToolStripStatusLabel,
            this.messagesToolStripStatusLabel});
            this.mainStatusStrip.Location = new System.Drawing.Point(0, 557);
            this.mainStatusStrip.Name = "mainStatusStrip";
            this.mainStatusStrip.Size = new System.Drawing.Size(784, 22);
            this.mainStatusStrip.TabIndex = 2;
            this.mainStatusStrip.Text = "statusStrip1";
            // 
            // timeToolStripStatusLabel
            // 
            this.timeToolStripStatusLabel.Name = "timeToolStripStatusLabel";
            this.timeToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // fileCountToolStripStatusLabel
            // 
            this.fileCountToolStripStatusLabel.Name = "fileCountToolStripStatusLabel";
            this.fileCountToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // currentFileToolStripStatusLabel
            // 
            this.currentFileToolStripStatusLabel.Name = "currentFileToolStripStatusLabel";
            this.currentFileToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // messagesToolStripStatusLabel
            // 
            this.messagesToolStripStatusLabel.Name = "messagesToolStripStatusLabel";
            this.messagesToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // baseFolderBrowserDialog
            // 
            this.baseFolderBrowserDialog.ShowNewFolderButton = false;
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(784, 24);
            this.mainMenuStrip.TabIndex = 3;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.aboutToolStripMenuItem.Text = "О программе";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 579);
            this.Controls.Add(this.mainStatusStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.Controls.Add(this.resultsGroupBox);
            this.Controls.Add(this.parametersGroupBox);
            this.MainMenuStrip = this.mainMenuStrip;
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "MainForm";
            this.Text = "Файловый поиск";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.parametersGroupBox.ResumeLayout(false);
            this.parametersGroupBox.PerformLayout();
            this.resultsGroupBox.ResumeLayout(false);
            this.mainStatusStrip.ResumeLayout(false);
            this.mainStatusStrip.PerformLayout();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox parametersGroupBox;
        private System.Windows.Forms.GroupBox resultsGroupBox;
        private System.Windows.Forms.TreeView resultsTreeView;
        private System.Windows.Forms.StatusStrip mainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel currentFileToolStripStatusLabel;
        private System.Windows.Forms.Button browseBaseDirectoryButton;
        private System.Windows.Forms.TextBox baseDirectoryPathTextBox;
        private System.Windows.Forms.Label baseDirectoryPathLabel;
        private System.Windows.Forms.TextBox innerTextTextBox;
        private System.Windows.Forms.Label innerTextLabel;
        private System.Windows.Forms.TextBox fileNamePatternTextBox;
        private System.Windows.Forms.Label fileNamePatternLabel;
        private System.Windows.Forms.ToolStripStatusLabel timeToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel fileCountToolStripStatusLabel;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button startStopButton;
        private System.Windows.Forms.FolderBrowserDialog baseFolderBrowserDialog;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.ToolStripStatusLabel messagesToolStripStatusLabel;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

