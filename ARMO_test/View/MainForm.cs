﻿using ARMO_test.Controller;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ARMO_test.View
{
    public partial class MainForm : Form, IMainView
    {
        private IMainController mController;
        private MessageType mMessages;
        private Dictionary<string, TreeNode> mResultNodes;

        public string BaseDirectoryPath
        {
            get { return baseDirectoryPathTextBox.Text; }
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke(new MethodInvoker(() => { BaseDirectoryPath = value; }));
                    return;
                }

                baseDirectoryPathTextBox.Text = value;
            }
        }

        public string FilenamePattern
        {
            get { return fileNamePatternTextBox.Text; }
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke(new MethodInvoker(() => { FilenamePattern = value; }));
                    return;
                }

                fileNamePatternTextBox.Text = value;
            }
        }

        public string InnerText
        {
            get { return innerTextTextBox.Text; }
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke(new MethodInvoker(() => { InnerText = value; }));
                    return;
                }

                innerTextTextBox.Text = value;
            }
        }

        public MessageType Messages
        {
            get { return mMessages; }
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke(new MethodInvoker(() => { Messages = value; }));
                    return;
                }

                mMessages = value;

                UpdateMessagesText();
            }
        }

        public MainForm()
        {
            mMessages = MessageType.MessageTypeNoMessage;
            mResultNodes = new Dictionary<string, TreeNode>();

            InitializeComponent();
        }

        public void SetController(IMainController value)
        {
            mController = value;
        }

        public void SetCurrentProcessingFilename(string value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(() => SetCurrentProcessingFilename(value)));
                return;
            }
            else
            {
                if (string.IsNullOrEmpty(value))
                {
                    currentFileToolStripStatusLabel.Text = "";
                }
                else
                {
                    currentFileToolStripStatusLabel.Text = $"{Resources.textCurrentFile}: {value}";
                }
            }
        }

        public void SetProcessedFileCount(int value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(() => SetProcessedFileCount(value)));
                return;
            }

            fileCountToolStripStatusLabel.Text = $"{Resources.textFileCount}: {value}";
        }

        public void SetSearchTime(int value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(() => SetSearchTime(value)));
                return;
            }

            timeToolStripStatusLabel.Text = $"{Resources.textTimeElapsed}: {value}с.";
        }

        public void SetMode(MainViewMode value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(() => SetMode(value)));
                return;
            }

            switch (value)
            {
                // Убирает кнопку паузы и позволяет начать поиск
                case MainViewMode.ModeStopped:
                    {
                        startStopButton.Text = Resources.textButtonStart;
                        pauseButton.Visible = false;
                        pauseButton.Enabled = false;

                        break;
                    }
                // Включает кнопку паузы и позволяет остановить поиск
                case MainViewMode.ModeRunning:
                    {
                        startStopButton.Text = Resources.textButtonStop;
                        pauseButton.Visible = true;
                        pauseButton.Enabled = true;

                        break;
                    }
                // Выключает кнопку паузы
                case MainViewMode.ModePaused:
                    {
                        startStopButton.Text = Resources.textButtonStart;
                        pauseButton.Visible = true;
                        pauseButton.Enabled = false;

                        break;
                    }
            }
        }

        public void AddResult(string parent, string key, string name)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(() => AddResult(parent, key, name)));
                return;
            }

            // Выход, если результат уже есть
            if (mResultNodes.ContainsKey(key))
            {
                return;
            }

            TreeNode parentNode = null;
            TreeNodeCollection attachTo;

            // Выбор родительского элемента для результата - общий список или конкретный элемент
            if (string.IsNullOrEmpty(parent))
            {
                attachTo = resultsTreeView.Nodes;
            }
            else
            {
                parentNode = mResultNodes[parent];

                attachTo = mResultNodes[parent].Nodes;
            }

            // Добавление отображения нового результата
            TreeNode node = attachTo.Add(name);

            mResultNodes.Add(key, node);

            parentNode?.Expand();
        }

        public void ResetResults()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(() => ResetResults()));
                return;
            }

            timeToolStripStatusLabel.Text = "";

            resultsTreeView.Nodes.Clear();
            mResultNodes.Clear();
        }

        /// <summary>
        /// Обновляет статусные сообщения.
        /// </summary>
        private void UpdateMessagesText()
        {
            if (mMessages == MessageType.MessageTypeNoMessage)
            {
                messagesToolStripStatusLabel.Text = "";
                return;
            }

            string text = "";

            if (mMessages.HasFlag(MessageType.MessageTypeNoSuchDirectory))
            {
                text = string.IsNullOrEmpty(text) ? Resources.messageNoSuchDirectory : $"{text} / {Resources.messageNoSuchDirectory}";
            }

            if (mMessages.HasFlag(MessageType.MessageSearchFinished))
            {
                text = string.IsNullOrEmpty(text) ? Resources.messageSearchFinished : $"{text} / {Resources.messageSearchFinished}";
            }

            if (mMessages.HasFlag(MessageType.MessageSearchPaused))
            {
                text = string.IsNullOrEmpty(text) ? Resources.messageSearchPaused : $"{text} / {Resources.messageSearchPaused}";
            }

            if (mMessages.HasFlag(MessageType.MessageTypeNoParameters))
            {
                text = string.IsNullOrEmpty(text) ? Resources.messageNoParameters : $"{text} / {Resources.messageNoParameters}";
            }

            if (mMessages.HasFlag(MessageType.MessageTypePatternError))
            {
                text = string.IsNullOrEmpty(text) ? Resources.messagePatternError : $"{text} / {Resources.messagePatternError}";
            }

            if (mMessages.HasFlag(MessageType.MessageTypeBaseDirectoryNoAccess))
            {
                text = string.IsNullOrEmpty(text) ? Resources.messageBaseDirectoryNoAccess : $"{text} / {Resources.messageBaseDirectoryNoAccess}";
            }

            messagesToolStripStatusLabel.Text = text;
        }

        /// <summary>
        /// Дает пользователю возможность выбрать начальную директорию для поиска.
        /// </summary>
        private void browseBaseDirectoryButton_Click(object sender, EventArgs e)
        {
            DialogResult result = baseFolderBrowserDialog.ShowDialog();

            string path = baseFolderBrowserDialog.SelectedPath;

            if (result != DialogResult.OK || string.IsNullOrEmpty(path))
            {
                return;
            }

            baseDirectoryPathTextBox.Text = path;

            mController?.BaseDirectoryChanged(path);
        }

        /// <summary>
        /// Уведомляет контроллер о изменившейся начальной директории.
        /// </summary>
        private void baseDirectoryPathTextBox_TextChanged(object sender, EventArgs e)
        {
            messageLabel.Text = "";

            mController?.BaseDirectoryChanged(baseDirectoryPathTextBox.Text);
        }

        /// <summary>
        /// Уведомляет контроллер о старте/остановке поиска.
        /// </summary>
        private void startStopButton_Click(object sender, EventArgs e)
        {
            mController?.StartStopSearch();
        }

        /// <summary>
        /// Уведомляет контроллер о приостановке поиска.
        /// </summary>
        private void pauseButton_Click(object sender, EventArgs e)
        {
            mController?.PauseSearch();
        }

        /// <summary>
        /// Уведомляет контроллер о закрытии окна.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mController?.HandleClose();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var aboutForm = new AboutForm();
            aboutForm.Show();
        }
    }
}
