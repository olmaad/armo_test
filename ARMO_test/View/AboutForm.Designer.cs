﻿namespace ARMO_test.View
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.aboutLabelTop = new System.Windows.Forms.Label();
            this.aboutLabelBottom = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // aboutLabelTop
            // 
            this.aboutLabelTop.AutoSize = true;
            this.aboutLabelTop.Location = new System.Drawing.Point(13, 13);
            this.aboutLabelTop.Name = "aboutLabelTop";
            this.aboutLabelTop.Size = new System.Drawing.Size(174, 13);
            this.aboutLabelTop.TabIndex = 0;
            this.aboutLabelTop.Text = "Тестовое задание для ГК АРМО.";
            // 
            // aboutLabelBottom
            // 
            this.aboutLabelBottom.AutoSize = true;
            this.aboutLabelBottom.Location = new System.Drawing.Point(16, 30);
            this.aboutLabelBottom.Name = "aboutLabelBottom";
            this.aboutLabelBottom.Size = new System.Drawing.Size(101, 13);
            this.aboutLabelBottom.TabIndex = 1;
            this.aboutLabelBottom.Text = "Мальц Олег, 2019.";
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 61);
            this.Controls.Add(this.aboutLabelBottom);
            this.Controls.Add(this.aboutLabelTop);
            this.MaximumSize = new System.Drawing.Size(300, 100);
            this.MinimumSize = new System.Drawing.Size(300, 100);
            this.Name = "AboutForm";
            this.Text = "О программе";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label aboutLabelTop;
        private System.Windows.Forms.Label aboutLabelBottom;
    }
}