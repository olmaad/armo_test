﻿using ARMO_test.Controller;
using ARMO_test.Model;
using ARMO_test.Search;
using ARMO_test.View;
using System;
using System.Windows.Forms;

namespace ARMO_test
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            var searchRunner = new SearchRunner();
            var searchResultStorage = new SearchResultStorage(searchRunner);
            var mainForm = new MainForm();
            var mainController = new MainController(mainForm, searchRunner, searchResultStorage);

            // Запуск потока исполнителя поиска
            searchRunner.Start();

            Application.Run(mainForm);

            // Остановка исполнителя
            searchRunner.Stop();
        }
    }
}
